import java.util.ArrayList;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.IOException;

/**
 * Kelas utama untuk menginisiasi program
 * @author Rico Putra Pradana
 * NPM 		: 1606892163
 * KELAS	: SDA D
 */
public class SDA1606892163L6A {
	public static void main(String[] args) throws IOException {
		//TODO read the input command
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter out = new PrintWriter(System.out);
		
	}

}

/**
 * Kelas untuk melakukan semua operasi pada program ini
 * @author Rico Putra Pradana
 * @version 06/11/2017 14.17
 */
class Solution {
	private BufferedReader in;
	private PrintWriter out;
	private MyPriorityQueue q;
	
	public Solution(BufferedReader input, PrintWriter output) {
		this.in = input;
		this.out = output;
		this.q = new MyPriorityQueue();
	}

	/**
	 * Method yang akan dijalankan sebagai method utama program
	 * @throws IOException - exception bila terjadi error pada proses Input/Output
	 */
	public void main() throws IOException
	{
		String input = in.readLine();
		while (input != null) {
			String[] command = input.split(" ");
			
			if (command[0].equalsIgnoreCase("TAMBAH")) {
				if (command[2].equalsIgnoreCase("UJI")) {
					Gas gas = new Gas("UJI", Integer.parseInt(command[3]));
				}
				else {
					
				}
			}
			else if (command[0].equalsIgnoreCase("KELUARKAN")) {
				
			}
			else if (command[0].equalsIgnoreCase("CEK")) {
				
			}
			else if (command[0].equalsIgnoreCase("DAFTAR")) {
				
			}
		}
	}
	
	/**
	 * Method untuk menambahkan massaJenis gas UJI ke dalam tabung
	 * @param massaJenis - nilai massa jenis yang dimasukkan
	 */
	public void addGasUji(Gas element)
	{
		q.add(element);
		out.println("Gas uji " + element.getMassaJenis() + " telah ditambahkan");
	}
	
	/**
	 * Method untuk mengeluarkan gas dari tabung
	 */
	public void removeGas()
	{
		if (q.isEmpty()) {
			System.out.println("Seluruh tabung kosong");
		}
		else {
			Gas top = q.poll();
			System.out.println("Gas " + top.getMassaJenis() + " telah dikeluarkan");
		}
	}
	
	/**
	 * Method untuk mengecek massa jenis pada tabung penelitian paling atas
	 */
	public void cekMassaJenis()
	{
		if (q.isEmpty()) {
			out.println("Seluruh tabung kosong");
		}
		else {
			Gas top = q.peek();
			out.println("Gas pada tabung paling atas memiliki massa jenis " + top.getMassaJenis());
		}
	}
	
	/**
	 * Method untuk menambahkan gas reaksi ke dalam tabung
	 * @param element - gas yang akan dimasukkan ke dalam tabung
	 */
	public void addGasReaksi(Gas element)
	{
		ArrayList<Gas> gasYangBertukarPosisi = q.add(element);
		
		if (!gasYangBertukarPosisi.isEmpty()) {
			String cetak = "";
			for (int i = 0; i < gasYangBertukarPosisi.size(); i++) {
				cetak += gasYangBertukarPosisi.get(i) + " ";
			}
			cetak += "menjadi gas reaksi";
			out.println(cetak);
		}
	}
	
	public void cetakDaftarGasReaksi()
	{
		if (!q.getGasYangBertukarPosisi().isEmpty()) {
			String cetak = "";
			for (int i = 0; i < q.getGasYangBertukarPosisi().size(); i++) {
				if (i < q.getGasYangBertukarPosisi().size()-1) {
					cetak += q.getGasYangBertukarPosisi().get(i).getMassaJenis() + " ";
				}
				else {
					cetak += q.getGasYangBertukarPosisi();
				}
			}
			out.println(cetak);
		}
		else {
			out.println("Tidak ada gas reaksi");
		}
		
	}
}


/**
 * Kelas yang merepresentasikan Gas yang akan dimasukkan ke dalam tabung
 * @author Rico Putra Pradana
 */
class Gas implements Comparable<Gas> {
	private String jenisGas;
	private int massaJenis;
	
	public Gas(String jenis, int massaJenis) {
		this.jenisGas = jenis;
		this.massaJenis = massaJenis;
	}
	
	public int getMassaJenis()
	{
		return massaJenis;
	}
	
	public void setMassaJenis(int rho)
	{
		this.massaJenis = rho;
	}
	
	public String getJenisGas()
	{
		return jenisGas;
	}
	
	public void setJenisGas(String jenis)
	{
		this.jenisGas = jenis;
	}
	
	public int compareTo(Gas gas)
	{
		return Integer.compare(this.massaJenis, gas.getMassaJenis());
	}
}

/**
 * Priority Queue implemented with Min Binary Heap
 * Please read the tutorial handout for the
 * implementation guidance
 * 
 * To check what parts that have to be completed,
 * 
 * @author M. Reza Qorib
 * @since 1.7
 * @version 1.0
 */
class MyPriorityQueue {
	private static final int INITIAL_SIZE = 10;
	private ArrayList<Gas> arrData;
	private int size;
	private ArrayList<Gas> gasYangBertukarPosisi;
	
	/**
	 * Creates a PriorityQueue with the default
	 * initial capacity (10) that orders its elements
	 * according to their natural ordering.
	 */
	public MyPriorityQueue() {
		super();
		this.arrData = new ArrayList<Gas>(INITIAL_SIZE);
		this.size = 0;
	}
	
	public ArrayList<Gas> getGasYangBertukarPosisi()
	{
		return gasYangBertukarPosisi;
	}
	
	/**
	 * Retrieves, but does not remove, the head of this queue,
	 * or returns null if this queue is empty.
	 * 
	 * @return the head of this queue, or null if this queue is empty
	 */
	public Gas peek() {
		Gas root = null;
		if (!isEmpty()) {
			root = arrData.get(0);
		}
		return root;
	}
	
	/**
	 * Retrieves and removes the head of this queue,
	 * or returns null if this queue is empty.
	 * 
	 * @return the head of this queue, or null if this queue is empty
	 */
	public Gas poll() {
		Gas top = peek();
		swap(0, arrData.size()-1); // INGAT! kalo node nya cuma 1 gimana!
		arrData.remove(arrData.size()-1);
		percolateDown(0);
		
		return top;
	}

	/**
	 * Inserts the specified element into this priority queue.
	 * 
	 * @param element - the element to add
	 * @return true if the insert operation succeed, false if not.
	 */
	public ArrayList<Gas> add(Gas element) {
		ArrayList<Gas> gasYangBertukarPosisiSaatPenambahan = new ArrayList<Gas>();
		int index = arrData.size();
		arrData.add(element);
		percolateUp(index, gasYangBertukarPosisiSaatPenambahan);
		
		return gasYangBertukarPosisiSaatPenambahan;
	}
	
	/**
	 * Returns the number of elements in this collection.
	 * 
	 * @return the number of elements in this collection
	 */
	public int size() {
		return this.size;
	}
	
	/**
	 * Returns true if this collection contains no elements.
	 * 
	 * @return true if this collection contains no elements
	 */
	public boolean isEmpty() {
		return this.size == 0;
	}
	
	/**
	 * Returns an ArrayList containing all of the elements in this queue.
	 * The elements are in no particular order.
	 * 
	 * The returned array will be "safe" in that no references to it are maintained by this queue,
	 * but this method only does a shallow copy so all the element reference is still pointed to
	 * the same object. 
	 * 
	 * @return an array containing all of the elements in this queue
	 */
	public ArrayList<Gas> toArray() {
		return new ArrayList<Gas>(this.arrData);
	}
	
	/**
	 * Check whether an index is within the data array
	 * 
	 * @param index - the index that want to be checked
	 * @return true if the index is valid, false if not
	 */
	private boolean isOutOfBound(int index) {
		return index >= arrData.size();
	}
	
	/**
	 * Get the index of the parent node (abstract) of desired index
	 * 
	 * @param index - the index that want to know it's parent (kinda sad :") )
	 * @return index of the parent if found, -1 if not
	 */
	private int getParent(int index) {
		return (index - 1) / 2;
	}
	
	/**
	 * Get the index of the right child node (abstract) of desired index
	 * @param index - the index that want to know it's right child
	 * @return index of the right child if found, -1 if not
	 */
	private int getRightChild(int index) {
		return 2 * (index + 1);
	}
	
	/**
	 * Get the index of the left child node (abstract) of desired index
	 * 
	 * @param index - the index that want to know it's left child
	 * @return index of the left child if found, -1 if not
	 */
	private int getLeftChild(int index) {
		return (2 * index) + 1;
	}

	/**
	 * Method to percolate up the element in desired index
	 * @param index - the index of the element to percolate up
	 */
	private void percolateUp(int index, ArrayList<Gas> gas) {
		// Check whether the index valid
		if (isOutOfBound(index)) {
			return;
		}
		// Get the required data
		int parentIndex = getParent(index);
		Gas parent = null;
		if (!isOutOfBound(parentIndex)) {
			parent = arrData.get(parentIndex);
		}
		Gas current = arrData.get(index);
		
		// Compare the value of gathered data
		if (current.compareTo(parent) < 0) {
			if (current.getJenisGas().equalsIgnoreCase("REAKSI")) {
				parent.setJenisGas("REAKSI");
			}
			gas.add(parent);
			gasYangBertukarPosisi.add(parent);
			swap(index, parentIndex);
			percolateUp(parentIndex, gas);
		} else {
			return;
		}
	}
	
	/**
	 * Method to percolate down the element in desired index
	 * @param index - the index of the element to percolate down
	 */
	private void percolateDown(int index) {
		if (isOutOfBound(index)) {
			return;
		}
		int leftChildIndex = getLeftChild(index);
		int rightChildIndex = getRightChild(index);
		Gas current = arrData.get(index);
		Gas leftChild = null;
		Gas rightChild = null;
		
		if (!isOutOfBound(leftChildIndex)) {
			leftChild = arrData.get(leftChildIndex);
		}
		
		if (!isOutOfBound(rightChildIndex)) {
			rightChild = arrData.get(rightChildIndex);
		}
		
		if (leftChild.compareTo(rightChild) < 0) {
			if (leftChild.compareTo(current) < 0) {
				swap(index, leftChildIndex);
				percolateDown(leftChildIndex);
			}
			else {
				return;
			}
		}
		else {
			if (rightChild.compareTo(current) < 0) {
				swap(index, rightChildIndex);
				percolateDown(rightChildIndex);
			}
		}
	}
	
	/**
	 * Swap the element of two indexes
	 * 
	 * @param index1 - the first index to be swapped
	 * @param index2 - the second index to be swapped
	 * @return true if the swap succeed, false if not
	 */
	private boolean swap(int index1, int index2) {
		if (isOutOfBound(index1) || isOutOfBound(index2)){
			return false;
		}
		
		Gas temp = arrData.get(index1);
		arrData.set(index1, arrData.get(index2));
		arrData.set(index2, temp);
		
		return true;
	}
	
	/**
	 * Method untuk mengatur jenis gas yang ditambahkan
	 * @param jenis - jenis gas yang ditambahkan
	 * @param indexGas - index
	 */
	public void setJenisGas(String jenis, int indexGas)
	{
		this.arrData.get(indexGas).setJenisGas(jenis);
	}
}
